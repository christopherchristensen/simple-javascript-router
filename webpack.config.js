const path = require("path");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const webpack = require("webpack");
const inProduction = (process.env.NODE_ENV === "production");

module.exports = {

    entry: ["babel-polyfill", "./resources/js/app.js"],

    output: {

        filename: "./app.js",
        path: path.resolve(__dirname, "public/dist")

    },

    optimization: {

        minimize: inProduction

    },

    plugins: [

        new MiniCssExtractPlugin({
            filename: "[name].css",
            chunkFilename: "[id].css"
        }),

        new webpack.LoaderOptionsPlugin({
            minimize: inProduction
        })

    ],

    module: {

        rules: [

            { // CSS LOADERS

                test: /\.s[ac]ss$/,

                use: [

                    MiniCssExtractPlugin.loader,
                    "css-loader", // Enables CSS but doesn't apply (option: style-loader)
                    "sass-loader" // Applies

                ]

            },

            {

                test: (inProduction ? /\.js$/ : undefined),

                exclude: (inProduction ? /node_modules/ : undefined),

                loader: (inProduction ? "babel-loader" : undefined),

            }

        ]
    }
};
