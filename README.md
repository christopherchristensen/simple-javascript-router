# Simple Javascript Router

A simple router in Vanilla Javascript.



## Installation

```bash
npm install simple-javascript-router
```



## Dependencies

The Simple Javascript Router depends on the following packages:

* axios



## Setup: Javascript
Since the Simple Javascript Router is so simplistic it really only works in the way described in this document. Follow the examples and it will also work in your environment.

The Router works by mapping routes to View objects so that when a
route is clicked (`<a href="this/route">Route</a>`) that page is fetched
from the server and fed into the `<view></view>` or `<div id="view"></div>` element.



### Mapping Routes to Views
If you follow these steps your router will fetch the view content from your server with the mapped path (e.g. "en/about") and automatically load it into the mapped View object with every click on an anchor tag (e.g. `<a href="en/about">About</a>`).

```javascript
/* Require package */
const sjr = require("simple-javascript-router");

/* Get essential Router components from package */
const Router = sjr.Router;
const View = sjr.View;

/* Create Router instance */
let router = new Router();

/* Map routes to Views */
router.map([ "en/about" ]).to(new View("about"));

router.map([ 
    
    "en/contact/route1", 
    "en/contact/route2" 
    
]).to(new View("contact"));
```



### Add extra functionality to routing

If you want to add some extra functionality when routing then you can add that functionality with the `addClientNavigation`:

```javascript
/* pathname is passed as parameter to use */
router.addClientNavigation((pathname, eventTarget) => {
    
    // To some custom task when routing
    
});
```

> **Warning:** If you add an action with this method it will be executed **after** the router has fetched the view data from the server and **not before**.



### After view load callbacks

If you want to run some Javascript everytime a View is loaded then you can do this:

```javascript
router.views["about"].onLoad(() => {
    
   // Do something after the view has been loaded
   // (e.g. bind an EventListener to a DOMElement) 
    
});
```



### Run current view callbacks

If you are rendering your pages on the server side when first loading your website then you can use the function `runCurrentView` to run the Javascript (see example above) that was defined in the `onLoad` method
after the html content has been loaded.

```javascript
router.runCurrentView();
```


### Using wildcards in path

Sometimes you have a path that varies in some parts so that the server can use the varying parts as parameters. This router does accept wildcards but **does not use them as parameters.** To add a path with one or more wildcards you can just add a wildcard `*` at the appropriate position in the path during mapping:

```javascript
router.map([ "*/about" ]).to(new View("about-with-wildcard"));
```



> **Warning**: When mapping with wildcards the order in which you map the routes is of importance. The shorter routes with wildcards need to come last. For instance:
>
> * `router.map([ "*/about" ]).to(new View("about-with-wildcard"));` 
> * `router.map([ "*" ]).to(new View("about-with-wildcard"));` 
>
> The order above works. The order below doesn't:
>
> * `router.map([ "*" ]).to(new View("about-with-wildcard"));` 
> * `router.map([ "*/about" ]).to(new View("about-with-wildcard"));` 



## Setup: HTML

In your body you need to place a `<view>` tag or a `<div>` with an id of "view". You can only place one `<view>` tag, since the idea is to load entire pages and not partials. You can also add a `<div>` with the id of "progress-bar" and the router will then use this progress bar to show the progress of the router.

```html
<!doctype html>
<html lang="en">
<head>
    
    <title>Your page</title>

</head>
<body>
  
    <view></view>
    
    <!-- or (exclusively) -->
    
    <div id="view"></div>
    
    <div id="progress-bar"></div>
	
  
</body>
</html>
```



## Setup: CSS

The router uses a progress bar as long as it is provided by you (see **Setup: HTML**). With this styling everything should run as expected:



```css
#progress-bar {
    position: fixed;
    height: 5px;
    top: 0;
    z-index: 2000;
    background-color: $color; /* Your color */
    -webkit-transition: width 0.5s;
    -moz-transition: width 0.5s;
    -ms-transition: width 0.5s;
    -o-transition: width 0.5s;
    transition: width 0.5s;
    visibility: hidden;
}

#progress-bar.active {
    visibility: visible;
}
```





## Setup: SASS

The router uses a progress bar as long as it is provided by you (see **Setup: HTML**). With this styling everything should run as expected:



```scss
#progress-bar {
    position: fixed;
    height: 5px;
    top: 0;
    z-index: 2000;
    background-color: $color; /* Your color */
    -webkit-transition: width 0.5s;
    -moz-transition: width 0.5s;
    -ms-transition: width 0.5s;
    -o-transition: width 0.5s;
    transition: width 0.5s;
    visibility: hidden;

    &.active {
        visibility: visible;
    }
}
```

