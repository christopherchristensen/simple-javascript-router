import Router from "./../modules/Router";

test('map one route to a view', () => {

    /* --- Arrange --- */
    let router = new Router();
    let mockView = {};

    mockView.name = 'profile';
    mockView.isView = true;

    /* --- Act --- */
    router.map(['profile']).to(mockView);

    /* --- Assert --- */
    expect(router.views['profile']).toBe(mockView);
    expect(router.routes).toEqual({profile: "profile"});

});

test('map 3 routes to a view', () => {

    /* --- Arrange --- */
    let router = new Router();
    let mockView = {};

    mockView.name = 'profile';
    mockView.isView = true;

    /* --- Act --- */
    router.map([

        'profile',
        'other/profile/path',
        'random/profile/path'

    ]).to(mockView);

    /* --- Assert --- */
    expect(router.views['profile']).toBe(mockView);

    expect(router.routes).toEqual({

        'profile': 'profile',
        'other/profile/path': 'profile',
        'random/profile/path': 'profile'

    });

});

test('map string to view', () => {

    /* --- Arrange --- */
    let router = new Router();
    let mockView = {};

    mockView.name = 'profile';
    mockView.isView = true;

    /* --- Act --- */
    router.map('profile/path').to(mockView);

    /* --- Assert --- */
    expect(router.views['profile']).toBe(mockView);
    expect(router.routes).toEqual({'profile/path': 'profile'});

});

test('map number to view', () => {

    /* --- Arrange --- */
    let router = new Router();
    let mockView = {};

    mockView.name = 'profile';
    mockView.isView = true;

    function mapToView () {
        router.map(123).to(mockView)
    }

    /* --- Act & Assert --- */
    expect(mapToView).toThrow(DOMException);

});

test('map object to view', () => {

    /* --- Arrange --- */
    let router = new Router();
    let mockView = {};

    mockView.name = 'profile';
    mockView.isView = true;

    function mapToView () {
        router.map({profile: 'profile'}).to(mockView)
    }

    /* --- Act & Assert --- */
    expect(mapToView).toThrow(DOMException);

});

test('no route given to map view to', () => {

    /* --- Arrange --- */
    let router = new Router();
    let mockView = {};

    mockView.name = 'profile';
    mockView.isView = true;

    function mapToView () {
        router.map().to(mockView)
    }

    /* --- Act & Assert --- */
    expect(mapToView).toThrow(DOMException);

});

test('map a wildcard route to a view', () => {

    /* --- Arrange --- */
    let router = new Router();
    let mockView = {};

    mockView.name = 'profile';
    mockView.isView = true;

    /* --- Act --- */
    router.map([

        '*/profile/*',

    ]).to(mockView);

    /* --- Assert --- */
    expect(router.views['profile']).toBe(mockView);

    expect(router.routes).toEqual({

        '*/profile/*': 'profile',

    });

});

test('get a view from a wildcard route', () => {

    /* --- Arrange --- */
    let router = new Router();
    let mockView = {};
    let view;

    mockView.name = 'profile';
    mockView.isView = true;

    /* --- Act --- */
    router.map([

        '*/profile/*',
        '/profile/house'

    ]).to(mockView);

    view = router.getViewByPathName('random/profile/random');

    /* --- Assert --- */
    expect(view).toBe(mockView);

    expect(router.routes).toEqual({

        '*/profile/*': 'profile',
        'profile/house': 'profile'

    });

    expect(view).toEqual(mockView);

});

test('get a view from several wildcard routes', () => {

    /* --- Arrange --- */
    let router = new Router();
    let mockView1 = {}, mockView2 = {};
    let view;

    mockView1.name = 'profile';
    mockView1.isView = true;

    mockView2.name = 'profile/something';
    mockView2.isView = true;

    /* --- Act --- */
    router.map([

        '*/profile/*',
        '*/profile'

    ]).to(mockView1);

    router.map(['/profile/*']).to(mockView2);

    view = router.getViewByPathName('profile/random');

    /* --- Assert --- */
    expect(view).toEqual(mockView2);

});

test('get a view from route with many wildcards', () => {

    /* --- Arrange --- */
    let router = new Router();
    let mockView = {};
    let view;

    mockView.name = 'profile';
    mockView.isView = true;

    /* --- Act --- */
    router.map([

        '*/profile/*/*/random/random/*/',

    ]).to(mockView);

    view = router.getViewByPathName('wildcard/profile/wildcard/wildcard/random/random/wildcard');

    /* --- Assert --- */
    expect(view).toEqual(mockView);

});

test('add callback as correct type to clientNavigate attribute', () => {

    /* --- Arrange --- */
    let router = new Router();
    let callback = () => { return 5; };

    /* --- Arrange --- */
    router.addClientNavigation(callback);

    /* --- Act --- */
    expect(router.clientNavigateFunction()).toBe(5);

});
