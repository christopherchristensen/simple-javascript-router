import URI from "../modules/URI";

test('path has slashes on both ends', () => {

    /* --- Arrange --- */
    let untrimmedPath = "/en/profile/";
    let trimmedPath;

    /* --- Act --- */
    trimmedPath = URI.trimPath(untrimmedPath);

    /* --- Assert --- */
    expect(trimmedPath).toBe("en/profile");

});

test('path only has prepended slash', () => {

    /* --- Arrange --- */
    let untrimmedPath = "/en/profile";
    let trimmedPath;

    /* --- Act --- */
    trimmedPath = URI.trimPath(untrimmedPath);

    /* --- Assert --- */
    expect(trimmedPath).toBe("en/profile");

});

test('path only has trailing slash', () => {

    /* --- Arrange --- */
    let untrimmedPath = "en/profile/";
    let trimmedPath;

    /* --- Act --- */
    trimmedPath = URI.trimPath(untrimmedPath);

    /* --- Assert --- */
    expect(trimmedPath).toBe("en/profile");

});
