/**
 *--------------------------------------------------------------------------
 * ViewService
 *--------------------------------------------------------------------------
 *
 * A small service abstraction wrapper to get View content from the server
 * using the axios API.
 *
 * @author: Christopher J. Christensen
 * @version: 1.0.0
 *
 */

import Api from "./Api"

export default {

    getView (path) {

        return Api().get(path);

    }

}
