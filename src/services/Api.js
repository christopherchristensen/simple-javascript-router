import axios from "axios"
import ProgressBar from "../modules/ProgressBar";

let progress = new ProgressBar("progress-bar");

export default() => {

    return axios.create({

        baseURL: window.location.origin,
        onDownloadProgress: () => {

            if (!progress) { return false; }

            progress.show(75, progress.speeds.slow);

        },
        onUploadProgress: progressEvent => {

            if (!progress) { return false; }

            progress.show(75, progress.speeds.slow);

        },
        timeout: timeoutEvent => {

            if (!progress) { return false; }

            progress.hide(100);

        }

    });

}
