/**
 *--------------------------------------------------------------------------
 * Router
 *--------------------------------------------------------------------------
 *
 * This module handles client side routing in Javascript.
 * It listens to all anchor tags on the page and acts as
 * a wrapper with extra functionality.
 *
 * The Router has the following attributes:
 *
 * - viewCollection (object): holds all mapped View objects
 * - routeCollection (object): holds all defined routes
 * - clientNavigate (function): custom callback function executed on routing
 * - defaultDelayTime (int): delay before progress bar disappears after routing
 * - tmp (array): temporary storage that is used during mapping of routes
 *
 * @author Christopher J. Christensen
 * @version 1.0.0
 *
 */

import View from "./View";
import ViewService from "../services/ViewService";
import URI from "./URI";
import ProgressBar from "./ProgressBar";

export default class {

    constructor () {

        this.viewCollection = {};
        this.routeCollection = {};
        this.beforeNavigation = () => {};
        this.clientNavigate = () => {};
        this.defaultDelayTime = 3000;
        this.resetTmp();
        this.on();

    }

    get clientNavigateFunction  () {

        return this.clientNavigate;

    }

    get defaultDelay () {

        return this.defaultDelayTime;

    }

    get routes () {

        return this.routeCollection;

    }

    get views () {

        return this.viewCollection;

    }

    set defaultDelay (delay) {

        if (typeof delay !== "number") {

            return console.error("[ Router ] Delay time not a number.");

        }

        this.defaultDelayTime = delay;

    }

    /**
     * Custom functionality added by user that is executed every time
     * the Router routes to a given path.
     *
     * @callback (function)
     */
    addClientNavigation (callback) {

        if (typeof callback !== "function") {

            throw new DOMException(
                "\n\n[ Router ] Callback not a function. Also add pathname as Parameter of callback."
            );

        }

        this.clientNavigate = callback;
        return false;

    }

    /**
     * Custom functionality added by user that is executed before every time
     * the Router routes to a given path.
     *
     * @callback (function)
     */
    addBeforeNavigate (callback = () => {}) {

        if (typeof callback !== "function") {

            throw new DOMException(
                "\n\n[ Router ] Callback not a function. Also add pathname as Parameter of callback."
            );

        }

        this.beforeNavigateFunction = callback;

        return false;

    }

    /**
     * Get the view via pathname because all mapped paths point to a view.
     *
     * @param pathname (string => must be pre-mapped to view)
     */
    getViewByPathName (pathname) {

        let path     = URI.trimPath(pathname);
        let viewName = this.getViewName(path) || null;

        return this.views[viewName];

    }

    /**
     * Checks if pathname has a wildcard.
     *
     * @param pathname (string)
     *
     * @return boolean
     */
    hasWildcard (pathname) {

        return pathname.includes('*');

    }

    /**
     * Get the view from server with ViewService and load into view
     * via the pathname. The pathnames are mapped to the View objects.
     * That way you only need to pass the pathname.
     *
     * @param path (string => must be pre-mapped to view)
     * @param success (function)
     * @param error (function)
     *
     */
    loadContentFromServerIntoView (path, success, error) {

        ViewService.getView(URI.makeURL(path))

            .then((response) => {

                success(response);

            })
            .catch((response) => {

                error(response);

            });

    }

    /**
     * Temporarily stores given routes in the tmp attribute
     * and returns the attribute so that the to()-method
     * can then map those routes to a View.
     *
     * @param arrayOfRoutes (array or string)
     * @return tmp
     */
    map (arrayOfRoutes) {

        if (!arrayOfRoutes) {

            return this;

        }

        this.resetTmp();
        this.tmp = arrayOfRoutes;

        return this;

    }

    /**
     * Checks for a match in an array (regex).
     *
     * @param path
     *
     * @return string
     */
    getViewName (path) {

        let regexMatch = false;

        Object.keys(this.routes).forEach((key) => {

            let regexKey = key;

            // stop once one found, since forEach doesn't have a break keyword
            if (regexMatch) { return false; }

            if (this.hasWildcard(key)) {

                regexKey = this.replaceWildcardWithRegex(regexKey);

            }

            if (path.match(regexKey)) {

                path = URI.trimPath(key);
                regexMatch = true; // change regexMatch to true so it won't search any further

            }

        });

        return this.routes[path];

    }

    /*
     * Navigate to given path
     *
     */
    navigateTo (pathname, eventTarget, error) {

        let path     = URI.trimPath(pathname);
        let view     = this.getViewByPathName(path);
        let progress = new ProgressBar("progress-bar");

        if (!view) {

            throw new DOMException(
                "\n\n[ Router ] Couldn't find view. Make sure view was mapped properly."
            );

        }

        try {

            this.beforeNavigation();

        } catch (e) {}

        progress.show(20, progress.speeds.slow);

        if (view.hasContent) {

            this.clientNavigate(path, eventTarget); // CUSTOM
            view.show();
            progress.show(100, progress.speeds.fast);
            progress.hide(this.defaultDelay);
            URI.pushState(path);
            return false;

        }

        return this.loadContentFromServerIntoView(path, (response) => {

            this.clientNavigate(path, eventTarget); // CUSTOM
            view.content = response.data;
            view.show();
            progress.show(100, progress.speeds.fast);
            progress.hide(this.defaultDelay);
            URI.pushState(path);

        }, (response) => {

            progress.show(100, progress.speeds.fast);
            progress.hide(this.defaultDelay);
            error(response); // CUSTOM

        });

    }

    /**
     * Switch the router on by binding the click event to all anchor tags.
     */
    on () {

        let allAnchorTags = document.getElementsByTagName("a");

        for (let anchor of allAnchorTags) {

            anchor.addEventListener("click", (event) => {

                let pathname = event.target.pathname
                    || event.currentTarget.pathname || event.path[1].pathname;

                event.preventDefault();
                event.stopPropagation();
                event.stopImmediatePropagation();

                if (!pathname) {

                    throw new DOMException("...");

                }

                return this.navigateTo(pathname, event.target);

            });

        }

    }

    /**
     * If a path has a wildcard this function will
     * replace the wildcard (*) with a regex pattern (.*)
     * so that it ignores those parts of the path during
     * the matching of the pathname and stored routes
     *
     * @param pathname
     *
     * @return string (regexPath)
     *
     */
    replaceWildcardWithRegex (pathname) {

        // remove first wildcard
        let regexPath = pathname.replace('*', '.*');

        while (regexPath.includes('/*')) {

            regexPath = regexPath.replace('/*', '/.*');

        }

        return regexPath;

    }

    /**
     * Resets the tmp array that is used to temporarily store the routes
     * passed in the map-method() so that the to()-method can retrieve them.
     *
     * @return void
     */
    resetTmp () {

        this.tmp = null;

    }

    /**
     * Runs the current view's runMethod after the routing.
     */
    runCurrentView () {

        let pathname = URI.path();
        let view = this.getViewByPathName(URI.trimPath(pathname));

        view.run();

    }

    /**
     * Maps the routes passed as parameter in the map()-method
     * to the given View instance. That way the router can later
     * know which View instance to display when routing.
     *
     * @param view
     * @return boolean
     */
    to (view) {

        if (!view instanceof View) {

            this.resetTmp();
            throw new DOMException("\n\n[ Router ] No View passed to map to.")

        }

        if (typeof this.tmp === 'string') {

            this.routes[URI.trimPath(this.tmp)] = view.name;
            this.resetTmp();
            this.views[view.name] = view;
            return true;

        }

        if (!Array.isArray(this.tmp)) {

            throw new DOMException('[ Router ] Route was not mapped. Received unexpected or no parameter from map()');

        }

        for (let route of this.tmp) {

            this.routes[URI.trimPath(route)] = view.name;

        }

        this.resetTmp();
        this.views[view.name] = view;
        return true;

    }

}
