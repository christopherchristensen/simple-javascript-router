/**
 *--------------------------------------------------------------------------
 * View
 *--------------------------------------------------------------------------
 *
 * A wrapper class that contains all functionality to simplify the routing
 * of views.
 *
 * @author: Christopher J. Christensen
 * @version: 1.2.0
 *
 */

export default class {

    constructor (name, onLoadCallback = () => {}) {

        this.runMethod       = onLoadCallback;
        this.viewName        = name;
        this.viewElement     = document.getElementsByTagName("view")[0];
        this.viewContent     = "";

    }

    get content () {

        return this.viewContent;

    }

    get element () {

        return this.viewElement;

    }

    get hasContent () {

        return this.content.length;

    }

    get name () {

        return this.viewName;

    }

    set content (data) {

        let tmpDOM = document.createElement("div");
        let view;

        tmpDOM.innerHTML = data;
        view = tmpDOM.getElementsByTagName("view")[0].innerHTML;

        if (!view) {

            view = tmpDOM.getElementsByClassName("view")[0].innerHTML;

        }

        if (!view) {

            view = tmpDOM.getElementById("view").innerHTML;

        }

        if (!view) {

            view = data;

        }

        this.viewContent = view;

    }

    set onLoadMethod (method) {

        this.runMethod = method;

    }

    onLoad (callback = () => {}) {

        if (typeof callback !== "function") {

            throw new DOMException(
                "\n\n[ View ] Callback is not a function."
            );

        }

        return this.onLoadMethod = callback;

    }

    removeContent () {

        this.viewContent = "";

    }

    run () {

        return this.runMethod();

    }

    show () {

        this.element.innerHTML = this.content;
        this.run();

    }

}
