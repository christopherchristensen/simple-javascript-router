/**
 *--------------------------------------------------------------------------
 * URI
 *--------------------------------------------------------------------------
 *
 * Handles the URL to make the routing easier. This modules only contains
 * static methods.
 *
 * @author: Christopher J. Christensen
 * @version: 1.0.0
 *
 */

export default class  {

    /**
     * Returns the language from the <head>
     * tag (lang="de"). The language is set
     * by PHP due to the URL on loading the
     * page.
     *
     * @return String (language)
     * */
    static language () {

        return document.getElementsByTagName("head")[0].lang;

    }

    /**
     * Add a callback that needs to be
     * executed after URL has changed.
     *
     * @param callback
     * */
    static listenToPopState (callback) {

        window.onpopstate = () => {

            if (!callback) {

                throw new DOMException("\n\n[ URI ] You can't listen to pop state without a callback function");

            }

            try {

                callback();

            } catch (error) {

                throw new DOMException("\n\n[ URI ] Callback on pop state couldn't not be executed properly");

            }

        }

    }

    /**
     * Adds the given path to url's origin
     *
     * @param path
     *
     * @return String (new url)
     * */
    static makeURL (path) {

        let trimPath = this.trimPath(path);
        return `${window.location.origin}/${trimPath}`;

    }

    /**
     * Updates URL to new path.
     * Takes baseURL into account.
     *
     * @param path
     *
     * */
    static pushState (path) {

        // setup full url with given path
        let url = this.makeURL(path);
        let pathName = path.charAt(0).toUpperCase() + path.slice(1);

        // add path to the history
        return window.history.pushState(

            {url: url, timestamp: Date.now(), path: path}, `${pathName}`, url + "/"

        );

    }

    /**
     * Return the current path from URL,
     * after it has been formatted and the
     * language tag has been removed.
     *
     * @return String (path)
     * */
    static path () {

        return this.trimPath(window.location.pathname);

    }

    /**
     * Removes slashes at the start and end
     * of given path. This method ignores
     * the format of the given path entirely
     * and only trims.
     *
     * @param path
     *
     * @return String (path)
     * */
    static trimPath (path) {

        let trimmedPath = path;

        if (!path && path !== "") {

            throw new DOMException("\n\n[ URI ] No path was given to trim");

        }

        // --- 1. remove slash at start, if exists
        if(path[0] === "/") {

            trimmedPath = trimmedPath.substring(1, trimmedPath.length);

        }
        // ---

        // --- 2. remove trailing slash, if exists
        if (path.substring(path.length - 1) === "/") {

            trimmedPath = trimmedPath.substring(0, trimmedPath.length - 1);

        }
        // ---

        return trimmedPath;

    }

}
