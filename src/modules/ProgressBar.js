/**
 * --------------------------------------------------------------------------
 *  ProgressBar
 * --------------------------------------------------------------------------
 *
 * This module works as a wrapper for a progress bar element in the DOM.
 * If you add a progress bar with the ID "progress-bar" the Router will
 * use that element during routing. Otherwise it will just ignore it.
 *
 * @author: Christopher J. Christensen
 * @version: 1.0.0
 */

export default class {

    constructor (elementID) {

        this.progressBarElement = document.getElementById(elementID);

        this.transitionSpeeds = {
            none: "transition-width-none",
            lightning: "transition-width-lightning",
            fast: "transition-width-fast",
            medium: "transition-width-medium",
            slow: "transition-width-slow",
            snail: "transition-width-snail"
        };

    }

    get progressBar () {

        return this.progressBarElement;

    }

    get speeds () {

        return this.transitionSpeeds;

    }

    /**
     * ProgressBar will be hidden if it exists
     * in the DOM. You can also add a custom delay
     * so that the ProgressBar waits before it hides.
     *
     * @param delay
     */
    hide (delay = 0) {

        window.timoutsOnProgress = setTimeout(() => {

            if (!this.progressBar) { return false; }

            try {

                this.removeTransitions();

            } catch (e) {



            }

            this.progressBar.classList.add("transition-none");
            this.progressBar.classList.remove("active");
            this.progressBar.style.width = "0%";
            this.progressBar.classList.remove("transition-none");

        }, delay);

    }

    /**
     * Removes any existing ProgressBar specific transition-classes
     * from the progress bar element.
     */
    removeTransitions () {

        for (let transition in this.speeds) {

            if (!this.speeds.hasOwnProperty(transition)) { continue; }

            this.progressBar.classList.remove(this.speeds[transition]);

        }

    }

    /**
     * Restarts the ProgressBar from the beginning (width 0%)
     *
     * @param progress (int)
     * @param speed (string => transition-class-name)
     */
    restart (progress, speed) {

        this.hide(0);

        clearTimeout(window.timeoutsOnProgress);
        window.timeoutsOnProgress = setTimeout(() => {

            this.show(progress, speed);

        }, 10);

    }

    /**
     * Shows the ProgressBar element with the width of the progress
     * passed as first parameter. The transition speed determines
     * how fast the ProgressBar element will transition to the
     * given percentage (int).
     *
     * @param progress (int)
     * @param speed (string => transition-class-name)
     *
     */
    show (progress = 5, speed = "transition-none") {

        if (!progress) { progress = 5; } // in case 0

        try { this.removeTransitions(); } catch (e) {}

        window.timeoutsOnProgress = setTimeout(() => {

            this.progressBar.classList.add("active");
            this.progressBar.classList.add(speed);
            this.progressBar.style.width = progress + "%";

        }, 10);

    }

}
